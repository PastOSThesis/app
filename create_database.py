from app import app, db

with app.test_request_context():
    db.init_app(app)
    db.create_all()