from app import Todo
import pytest


@pytest.fixture(scope='module')
def new_todo():
    todo = Todo(content="Testing")
    return todo

def test_validate_todo1():
    
    data = {
        "content": "Hello"
    }
    todo = Todo(content=data["content"])
    
    assert todo is not None
    assert todo.content == data["content"]
    
def test_validate_todo2(new_todo):
    
    assert new_todo is not None
    assert new_todo.content == "Testing"